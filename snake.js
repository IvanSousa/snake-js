var canvas = document.getElementById('game');
var context = canvas.getContext('2d');

var grid = 16;
var count = 0;

class snake {
  constructor(weights){
    this.x = 160;
    this.y = 160;

    // snake velocity. moves one grid length every frame in either the x or y direction
    this.dx = grid;
    this.dy = 0;

    // keep track of all grids the snake body occupies
    this.cells = [];

    // length of the snake. grows when eating an apple
    this.maxCells = 4;

  }
}

var apple = {
  x: 320,
  y: 320
};

let currentPlayer = new snake(false);


// get random whole numbers in a specific range
// @see https://stackoverflow.com/a/1527820/2124254
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

// game loop
function loop() {
  requestAnimationFrame(loop);

  // slow game loop to 15 fps instead of 60 (60/15 = 4)
  if (++count < 5) {
    return;
  }

  count = 0;
  context.clearRect(0, 0, canvas.width, canvas.height);

  // move snake by it's velocity
  currentPlayer.x += currentPlayer.dx;
  currentPlayer.y += currentPlayer.dy;

  // wrap snake position horizontally on edge of screen
  // we use the resetGame function to simulate collision to the edge walls
  if (currentPlayer.x < 0) {
    // snake.x = canvas.width - grid;
    resetGame(); 
  } else if (currentPlayer.x >= canvas.width) {
    // snake.x = 0;
    resetGame();
  }

  // wrap snake position vertically on edge of screen
  if (currentPlayer.y < 0) {
    // snake.y = canvas.height - grid;
    resetGame();
  } else if (currentPlayer.y >= canvas.height) {
    // snake.y = 0;
    resetGame();
  }

  // keep track of where snake has been. front of the array is always the head
  currentPlayer.cells.unshift({
    x: currentPlayer.x,
    y: currentPlayer.y
  });

  // remove cells as we move away from them
  if (currentPlayer.cells.length > currentPlayer.maxCells) {
    currentPlayer.cells.pop();
  }

  // draw apple
  context.fillStyle = 'red';
  context.fillRect(apple.x, apple.y, grid - 1, grid - 1);

  // draw snake one cell at a time
  context.fillStyle = 'green';
  currentPlayer.cells.forEach(function(cell, index) {

    // drawing 1 px smaller than the grid creates a grid effect in the snake body so you can see how long it is
    context.fillRect(cell.x, cell.y, grid - 1, grid - 1);

    // snake ate apple
    if (cell.x === apple.x && cell.y === apple.y) {
      currentPlayer.maxCells++;
      // currentPlayer.deathTime = 100;

      // canvas is 400x400 which is 25x25 grids 
      apple.x = getRandomInt(0, 25) * grid;
      apple.y = getRandomInt(0, 25) * grid;
    }

    // check collision with all cells after this one (modified bubble sort)
    for (var i = index + 1; i < currentPlayer.cells.length; i++) {

      // snake occupies same space as a body part. reset game
      if (cell.x === currentPlayer.cells[i].x && cell.y === currentPlayer.cells[i].y) {
        resetGame();
      }
    }
  });

}

function resetGame() {
  currentPlayer = new snake(false);

  apple.x = getRandomInt(0, 25) * grid;
  apple.y = getRandomInt(0, 25) * grid;


}


// listen to keyboard events to move the snake
document.addEventListener('keydown', function(e) {
  // prevent snake from backtracking on itself by checking that it's 
  // not already moving on the same axis (pressing left while moving
  // left won't do anything, and pressing right while moving left
  // shouldn't let you collide with your own body)

  // left arrow key
  if (e.key === 'a' && currentPlayer.dx === 0) {
    currentPlayer.dx = -grid;
    currentPlayer.dy = 0;
  }
  // up arrow key
  else if (e.key === 'w' && currentPlayer.dy === 0) {
    currentPlayer.dy = -grid;
    currentPlayer.dx = 0;
  }
  // right arrow key
  else if (e.key === 'd' && currentPlayer.dx === 0) {
    currentPlayer.dx = grid;
    currentPlayer.dy = 0;
  }
  // down arrow key
  else if (e.key === 's' && currentPlayer.dy === 0) {
    currentPlayer.dy = grid;
    currentPlayer.dx = 0;
  }
});




// start the game
requestAnimationFrame(loop);