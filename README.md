# Snake js


## Name
The traditional snake game made in javascript.

## Description
This project was made with the purpose to practice my programming skills after a 1 year and half of no programming at all.


## Installation
Download the index.html and snake.js files and run the index.html file on a browser.


## License
This is open source, feel free to use the code for any of your projects.

## Project status
I will add machine learning to the game so it can played by itself in the future.